<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'title' => 'Administrador',
            'description' => 'Login de utilização do administrador'
        ]);
        DB::table('roles')->insert([
            'title' => 'Professor',
            'description' => 'Login de utilização do professor'
        ]);
        DB::table('roles')->insert([
            'title' => 'Aluno',
            'description' => 'Login de utilização do aluno'
        ]);
    }
}
