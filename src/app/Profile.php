<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'cpf', 'rg', 'birthdate'];
    protected $casts = ['birthdate' => 'date'];

    public function enrollments() { return $this->hasMany('App\Enrollment', 'student_id'); }
    public function attendances() { return $this->hasMany('App\Attendance', 'teacher_id'); }
    public function classes() { return $this->hasMany('App\Class', 'responsible_id'); }
    public function user() { return $this->belongsTo('App\User'); }
    public function role() { return $this->belongsTo('App\Role'); }
}
