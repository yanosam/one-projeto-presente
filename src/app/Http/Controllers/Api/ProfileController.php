<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Http\Resources\Profile as ProfileResource;
use App\Http\Resources\ProfileCollection;
use App\Profile; 

class ProfileController extends Controller
{
    public function index()
    {
        return new ProfileCollection(Profile::paginate(20));
    }   

    public function store(Request $request)
    {        
        $input = $request->all(); 
        if (!empty($input['birthdate'])) {
            $input['birthdate'] = \Carbon\Carbon::createFromFormat('d/m/Y', $input['birthdate'])->toDateString();
        }

        $profile = new Profile();
        $profile->name = $input['name'];
        $profile->email = $input['email'];
        $profile->phone = $input['phone'];
        $profile->cpf = $input['cpf'];
        $profile->rg = $input['rg'];
        $profile->birthdate = $input['birthdate'];
        $profile->save();
        
        return new ProfileResource($profile);
    }
    
    public function show($id)
    {
        return new ProfileResource(Profile::FindOrFail($id));
    }
    
    public function update(Request $request, $id)
    {
        $input = $request->all(); 
        if (!empty($input['birthdate'])) {
            $input['birthdate'] = \Carbon\Carbon::createFromFormat('d/m/Y', $input['birthdate'])->toDateString();
        }
        
        $profile = Profile::findOrFail($id);
        $profile->name = $input['name'];
        $profile->email = $input['email'];
        $profile->phone = $input['phone'];
        $profile->cpf = $input['cpf'];
        $profile->rg = $input['rg'];
        $profile->birthdate = $input['birthdate'];
        $profile->save();
        
        return new ProfileResource($profile);
    }
    
    public function destroy($id)
    {
        $profile = Profile::findOrFail($id);
        $profile->delete();
        
        return new ProfileResource($profile);

    }
}
