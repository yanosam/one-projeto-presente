<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth; 
use App\Http\Resources\Role as RoleResource;
use App\Http\Resources\RoleCollection;
use App\Role; 

class RoleController extends Controller
{
    public function index()
    {
        return new RoleCollection(Role::paginate(20));
    }   

    public function store(Request $request)
    {        
        $input = $request->all(); 

        $role = new Role();
        $role->title = $input['title'];
        $role->description = $input['description'];
        $role->save();
        
        return new RoleResource($role);
    }
    
    public function show($id)
    {
        return new RoleResource(Role::FindOrFail($id));
    }
    
    public function update(Request $request, $id)
    {
        $input = $request->all(); 
        
        $role = Role::findOrFail($id);
        $role->title = $input['title'];
        $role->description = $input['description'];
        $role->save();
        
        return new RoleResource($role);
    }
    
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        
        return new RoleResource($role);

    }
}
