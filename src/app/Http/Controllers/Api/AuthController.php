<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Profile; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;

class AuthController extends Controller
{
    public function register(Request $request) 
    {    
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email',
            'password' => 'required',  
            'c_password' => 'required|same:password', 
        ]); 

        if ($validator->fails()) return response()->json(['error'=>$validator->errors()], 401);

        DB::beginTransaction();
        try {
            $input = $request->all(); 
            if (!empty($input['birthdate'])) {
                $input['birthdate'] = \Carbon\Carbon::createFromFormat('d/m/Y', $input['birthdate'])->toDateString();
            }
            
            $profileData = [
                'name' => $input['name'],
                'email' => $input['email'],
                'phone' => @$input['phone'],
                'cpf' => @$input['cpf'],
                'rg' => @$input['rg'],
                'birthdate' => @$input['birthdate']
            ];

            $profile = Profile::create($profileData); 
            
            
            $input['password'] = bcrypt($input['password']);
            $userData = [
                'profile_id' => $profile->id,
                'username' => $input['name'],
                'email' => $input['email'],
                'password' => $input['password']
            ];
    
            $user = User::create($userData); 
            DB::commit();
            $success['token'] = $user->createToken('AppName')->accessToken;
            return response()->json(['success'=>$success], 200); 
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['success'=>false], 400); 
        }
   }
     

    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('AppName')->accessToken; 
            return response()->json(['success' => $success], 200); 
        } else{ 
            return response()->json(['error' => 'Unauthorised'], 401); 
        } 
    }
     
    public function getUser() {
        $user = Auth::user();
        return response()->json(['user' => $user], 200); 
    }
}
