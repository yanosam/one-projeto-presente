<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth; 
use App\Http\Resources\Subject as SubjectResource;
use App\Http\Resources\SubjectCollection;
use App\Subject; 

class SubjectController extends Controller
{
    public function index()
    {
        return new SubjectCollection(Subject::paginate(20));
    }   

    public function store(Request $request)
    {        
        $input = $request->all(); 

        $subject = new Subject();
        $role->title = $input['title'];
        $role->description = $input['description'];
        $subject->save();
        
        return new SubjectResource($subject);
    }
    
    public function show($id)
    {
        return new SubjectResource(Subject::FindOrFail($id));
    }
    
    public function update(Request $request, $id)
    {
        $input = $request->all(); 
        
        $subject = Subject::findOrFail($id);
        $role->title = $input['title'];
        $role->description = $input['description'];
        $subject->save();
        
        return new SubjectResource($subject);
    }
    
    public function destroy($id)
    {
        $subject = Subject::findOrFail($id);
        $subject->delete();
        
        return new SubjectResource($subject);

    }
}
