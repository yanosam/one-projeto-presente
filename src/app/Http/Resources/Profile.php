<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Profile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name, 
            'email' => $this->email, 
            'phone' => $this->phone, 
            'cpf' => $this->cpf, 
            'rg' => $this->rg, 
            'birthdate' => $this->birthdate
        ];
    }
}
