<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    protected $fillable = ['student_id', 'class_id', 'expired_at'];
    protected $casts = ['expired_at' => 'timestamp'];

    public function attendances() { return $this->hasMany('App\Attendance'); }
    public function student() { return $this->belongsTo('App\Prodile', 'student_id'); }
    public function class() { return $this->belongsTo('App\Class'); }

}
