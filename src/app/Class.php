<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Class extends Model
{
    protected $fillable = ['subject_id', 'responsible_id', 'title', 'description', 'capacity'];

    public function enrollments() { return $this->hasMany('App\Enrollment'); }
    public function subject() { return $this->belongsTo('App\Subject'); }
    public function responsible() { return $this->belongsTo('App\Profile'); }
}
