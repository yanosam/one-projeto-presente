<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = ['enrollment_id', 'teacher_id', 'amount'];

    public function enrollment() { return $this->belongsTo('App\Enrollment'); }
    public function teacher() { return $this->belongsTo('App\Profile', 'teacher_id'); }
}
