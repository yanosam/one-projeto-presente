<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function(){
    Route::post('login', 'Api\AuthController@login')->name('login');
    Route::post('register', 'Api\AuthController@register')->name('register');
    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('getUser', 'Api\AuthController@getUser')->name('getUser');
        Route::apiResource('profiles','Api\ProfileController');
        Route::apiResource('roles','Api\RoleController');
        Route::apiResource('subjects','Api\SubjectController');
    });
});
