FROM php:7.2-apache

WORKDIR /var/www

RUN apt-get update && apt-get -y install \
    acl \
    cron \
    curl \
    libicu-dev \
    libmcrypt-dev \
    libmemcached-dev \
    libpng-dev \
    libxml2-dev \
    libzip-dev \
    nano \
    zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install \
    bcmath \
    gd \
    intl \
    json \
    mbstring \
    pdo_mysql \
    xml \
    zip

RUN a2enmod rewrite ssl

ARG APACHECONF

COPY $APACHECONF /etc/apache2/sites-available/000-default.conf

RUN sed -ri -e ':a;N;$!ba; s/AllowOverride None/AllowOverride All/3' /etc/apache2/apache2.conf

COPY ./src .

COPY etc/default.ini /usr/local/etc/php/conf.d/default.ini

CMD ["apache2-foreground"]